import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    displayValue: "0"
  }
  render() {
    return (
      <div className="calculator">
        <div className="calculator-display"></div>
        <div className="keypad">
          <div className="function-keys">
            <button className="calculator-key key-clear" >AC</button>
            <button className="calculator-key key-sign" >±</button>
            <button className="calculator-key key-percent" >%</button>
          </div>
          <div className="digit-keys">
            <button className="calculator-key key-0" onClick={() => input(0)}>0</button>
            <button className="calculator-key key-dot" >.</button>
            <button className="calculator-key key-1"  >1</button>
            <button className="calculator-key key-2" >2</button>
            <button className="calculator-key key-3" >3</button>
            <button className="calculator-key key-4" >4</button>
            <button className="calculator-key key-5" >5</button>
            <button className="calculator-key key-6" >6</button>
            <button className="calculator-key key-7" >7</button>
            <button className="calculator-key key-8" >8</button>
            <button className="calculator-key key-9" >9</button>
          </div>
          <div className="operator-keys">
            <button className="calculator-key key-div" >÷</button>
            <button className="calculator-key key-multi" >x</button>
            <button className="calculator-key key-sub" >-</button>
            <button className="calculator-key key-add" >+</button>
            <button className="calculator-key key-equals" >=</button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
